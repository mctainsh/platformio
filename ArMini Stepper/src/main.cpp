#include <Arduino.h>

#define BUILTIN_LED 13

void blink(int delayMs);
void moveOnePeriod(int dir, int ms);
void moveSteps(int dir, int ms, int steps);

const int MotorPins[] = {14, 15, 16, 17};			//define pins connected to four phase ABCD of stepper motor
const int CCWStep[] = {0x01, 0x02, 0x04, 0x08}; 	//define power supply order for coil for rotating anticlockwise
const int CWStep[] = {0x08, 0x04, 0x02, 0x01};		//define power supply order for coil for rotating clockwise
int _count = 0;

void setup()
{
	Serial.begin(9600);
	Serial.println("Begin setup 1.0");

	pinMode(BUILTIN_LED, OUTPUT);

	// Steper motor setup
	for (int i = 0; i < 4; i++)
		pinMode(MotorPins[i], OUTPUT);

}

///////////////////////////////////////////////////////////////////////////////
// Min loop
void loop()
{
	// Blink
	for (int n = 0; n < 5; n++)
		blink(100);
	Serial.println("Hello from JOHN");
	for (int n = 0; n < 5; n++)
		blink(250);
	for (int n = 0; n < 5; n++)
		blink(50);
	Serial.println(millis());

	// Drive motor
	//moveSteps(1, 10, 512); //rotating 360° clockwise, a total of 2048 steps in a circle, namely, 512 cycles.
	//delay(1500);
	//moveSteps(0, 10, 512); //rotating 360° anticlockwise
	//delay(1500);
}

void blink(int delayMs)
{
	digitalWrite(BUILTIN_LED, HIGH);
	delay(delayMs);
	digitalWrite(BUILTIN_LED, LOW);
	delay(delayMs);
}

///////////////////////////////////////////////////////////////////////////////
// As for four phase stepping motor, four steps is a cycle. the function is 
// used to drive the stepping motor clockwise or anticlockwise to take four steps
void moveOnePeriod(int dir, int ms)
{
	int i = 0, j = 0;
	for (j = 0; j < 4; j++)
	{ 
		//cycle according to power supply order
		for (i = 0; i < 4; i++)
		{				  
			//assign to each pin, a total of 4 pins
			if (dir == 1) //power supply order clockwise
				digitalWrite(MotorPins[i], (CCWStep[j] == (1 << i)) ? HIGH : LOW);
			else //power supply order anticlockwise
				digitalWrite(MotorPins[i], (CWStep[j] == (1 << i)) ? HIGH : LOW);
			//printf("motorPin %d, %d \n", motorPins[i], digitalRead(motorPins[i]));
		}
		//printf("Step cycle!\n");
		if (ms < 3) //the delay can not be less than 3ms, otherwise it will exceed speed limit of the motor
			ms = 3;
		delay(ms);
	}
}

///////////////////////////////////////////////////////////////////////////////
// Continuous rotation function, the parameter steps specifies the rotation 
// cycles, every four steps is a cycle
void moveSteps(int dir, int ms, int steps)
{
	for (int i = 0; i < steps; i++)
		moveOnePeriod(dir, ms);
}

//function used to stop rotating
//void motorStop()
//{ 
//	for (int i = 0; i < 4; i++)
//		digitalWrite(motorPins[i], LOW);
//}
