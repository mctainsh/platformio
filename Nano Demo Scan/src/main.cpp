#include <Arduino.h>
#include <Wire.h>
//#include <spi.h>
//#include "Adafruit_SSD1306.h"


void blink( int span );
void I2CScanner();
int n;

///////////////////////////////////////////////////////////////////////////////
// Setup
void setup() 
{
	Serial.begin(9600);
	pinMode(LED_BUILTIN, OUTPUT);
}

void loop() 
{
	Serial.print("Start loop ");
	Serial.println(n++);
	blink( 100 );
	blink( 100 );
	blink( 100 );
	blink( 1000 );
	blink( 1000 );
	blink( 1000 );
	blink( 100 );
	blink( 100 );
	blink( 100 );

	delay( 2000 );
	I2CScanner();
}

///////////////////////////////////////////////////////////////////////////////
// Blink for a long or short period of time
void blink( int span )
{
	digitalWrite(LED_BUILTIN, HIGH);
	delay( span );
	digitalWrite(LED_BUILTIN, LOW);
	delay( 100 );
}

///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Scan for available devices
void I2CScanner ()
{
  Serial.println ();
  Serial.println ("I2C scanner. Scanning ...");
  byte count = 0;

  Wire.begin();
  for (byte i = 8; i < 120; i++)
  {
    Wire.beginTransmission (i);          // Begin I2C transmission Address (i)
    if (Wire.endTransmission () == 0)  // Receive 0 = success (ACK response) 
    {
      Serial.print ("Found address: ");
      Serial.print (i, DEC);
      Serial.print (" (0x");
      Serial.print (i, HEX);     // PCF8574 7 bit address
      Serial.println (")");
      count++;
    }
  }
  Serial.print ("Found ");      
  Serial.print (count, DEC);        // numbers of devices
  Serial.println (" device(s).");
}
