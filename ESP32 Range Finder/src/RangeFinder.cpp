/*
 * Blink
 * Turns on an LED on for one second,
 * then off for one second, repeatedly.
 */

#include <Arduino.h>
#include <Wire.h>
#include <Ultrasonic.h>
#include <TFT_eSPI.h>

String Comma( long val );

// Set LED_BUILTIN if it is not defined by Arduino framework
// #define LED_BUILTIN 2
//#define PIN_COUNT 13
//int ALL_PINS[PIN_COUNT] = { 12, 13, 15, LED_BUILTIN, 25, 26, 27, 32, 33, 36, 37, 38, 39 };

TFT_eSPI tft = TFT_eSPI(); // TFT object

//#define PIN_COUNT 2
//int ALL_PINS[PIN_COUNT] = { 12, LED_BUILTIN };

#define TRIG_PIN 2
#define ECHO_PIN 38
Ultrasonic _utralsonic(TRIG_PIN, ECHO_PIN);

void setup()
{
	Serial.begin(115299);

	// Initialise display
	tft.begin();
	tft.setTextDatum(TL_DATUM); // Middle centre datum
	tft.setRotation(3);
	tft.setFreeFont(&Orbitron_Light_32);
	tft.setCursor(0, 30);

	tft.fillScreen(TFT_BLUE);
	tft.setTextColor(TFT_WHITE, TFT_BLACK);
	tft.println("Starting");

	// initialize LED digital pin as an output.
	//for( int n = 0; n < PIN_COUNT; n++ )
	//	pinMode(n, OUTPUT);
	pinMode(TRIG_PIN, OUTPUT);
	pinMode(ECHO_PIN, INPUT);
}



int count = 1;
void loop()
{



	//String message = millis();
	//message.
	count++;
	Serial.println(Comma(count));
	//Serial.println(Comma(1));
	//Serial.println(Comma(12));
	//Serial.println(Comma(123));
	//Serial.println(Comma(1234));
	//Serial.println(Comma(12345));
	//Serial.println(Comma(123456));
	//Serial.println(Comma(1234567));
	//Serial.println(Comma(12345678));
	Serial.printf( "Distance %d cm\r\n", _utralsonic.distanceRead(CM));
	Serial.printf( "Pin %d => %d \r\n", ECHO_PIN, digitalRead(ECHO_PIN));

	tft.fillRect( 0, 60, tft.width(), 30, TFT_BLACK);
	tft.setCursor(0, 90);
	tft.printf( "%d cm\r", _utralsonic.distanceRead(CM));

	// turn the LED on (HIGH is the voltage level)
	//for( int n = 0; n < PIN_COUNT; n++ )
	//	digitalWrite(n, HIGH);
	// wait for a second
	//delay(500);
	// turn the LED off by making the voltage LOW
	//for( int n = 0; n < PIN_COUNT; n++ )
	//	digitalWrite(n, LOW);

	// wait for a second
	delay(100);
}



String Comma( long val )
{
	String result, number;
	number += val;
	int len = number.length();
	for( int n = 0; n < len; n++)
	{
		result += number[n];
		if( len-1 != n && (len-n) % 3 == 1 )
			result += ',';
	}
	return result;
}



