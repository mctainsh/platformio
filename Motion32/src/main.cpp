#include <Arduino.h>
#include <Wire.h>
/*
#include "I2Cdev.h"
#include "MPU6050.h"


// class default I2C address is 0x68
// specific I2C addresses may be passed as a parameter here
// AD0 low = 0x68 (default for InvenSense evaluation board)
// AD0 high = 0x69
MPU6050 accelgyro(104);

int16_t ax, ay, az;     //store acceleration data
int16_t gx, gy, gz;     //store gyroscope data

void Scanner ();

const int LED_PIN = 2;

///////////////////////////////////////////////////////////////////////////////
// Setup IO and comms
void setup() 
{
	Serial.begin(115200);
	Serial.print("Serial ready");

	pinMode(LED_PIN, OUTPUT);
	Scanner();

	Serial.println("I2C Setup");
	Wire.begin (21, 22);   // sda= GPIO_21 /scl= GPIO_22


	accelgyro.initialize();     //initialize MPU6050

    // verify connection
    Serial.println("Testing device connections...");
    Serial.println(accelgyro.testConnection() ? "MPU6050 connection successful" : "MPU6050 connection failed");

	Serial.println("Setup complete");
	Serial.println("-----------------------------------");
}

///////////////////////////////////////////////////////////////////////////////
// Scan for available devices
void Scanner ()
{
  Serial.println ();
  Serial.println ("I2C scanner. Scanning ...");
  byte count = 0;

  Wire.begin();
  for (byte i = 8; i < 120; i++)
  {
    Wire.beginTransmission (i);          // Begin I2C transmission Address (i)
    if (Wire.endTransmission () == 0)  // Receive 0 = success (ACK response) 
    {
      Serial.print ("Found address: ");
      Serial.print (i, DEC);
      Serial.print (" (0x");
      Serial.print (i, HEX);     // PCF8574 7 bit address
      Serial.println (")");
      count++;
    }
  }
  Serial.print ("Found ");      
  Serial.print (count, DEC);        // numbers of devices
  Serial.println (" device(s).");
}

///////////////////////////////////////////////////////////////////////////////
// Main loop
void loop() 
{
	 // read accel/gyro values of MPU6050
    accelgyro.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);
    // display accel/gyro x/y/z values
    //printf("a/g: %6hd %6hd %6hd   %6hd %6hd %6hd\n",ax,ay,az,gx,gy,gz);
    printf("a/g: %.2f g %.2f g %.2f g   %.2f d/s %.2f d/s %.2f d/s \n",(float)ax/16384,(float)ay/16384,(float)az/16384,
        (float)gx/131,(float)gy/131,(float)gz/131);
	delay( 1000 );
}



*/

const int MPU_ADDR = 0x68;								   // I2C address of the MPU-6050. If AD0 pin is set to HIGH, the I2C address will be 0x69.
int16_t accelerometer_x, accelerometer_y, accelerometer_z; // variables for accelerometer raw data
int16_t gyro_x, gyro_y, gyro_z;							   // variables for gyro raw data
int16_t temperature;									   // variables for temperature data

void setup()
{
	Serial.begin(115200);
	Wire.begin();
	Wire.beginTransmission(MPU_ADDR); // Begins a transmission to the I2C slave (GY-521 board)
	Wire.write(0x6B);				  // PWR_MGMT_1 register
	Wire.write(0);					  // set to zero (wakes up the MPU-6050)
	Wire.endTransmission(true);
}
void loop()
{
	double count = 10;
	long totalX = 0;
	long totalY = 0;
	long totalZ = 0;
	for (int n = 0; n < count; n++)
	{
		Wire.beginTransmission(MPU_ADDR);
		Wire.write(0x3B);						 // starting with register 0x3B (ACCEL_XOUT_H) [MPU-6000 and MPU-6050 Register Map and Descriptions Revision 4.2, p.40]
		Wire.endTransmission(false);			 // the parameter indicates that the Arduino will send a restart. As a result, the connection is kept active.
		Wire.requestFrom(MPU_ADDR, 7 * 2, true); // request a total of 7*2=14 registers
		// "Wire.read()<<8 | Wire.read();" means two registers are read and stored in the same variable
		accelerometer_x = Wire.read() << 8 | Wire.read(); // reading registers: 0x3B (ACCEL_XOUT_H) and 0x3C (ACCEL_XOUT_L)
		accelerometer_y = Wire.read() << 8 | Wire.read(); // reading registers: 0x3D (ACCEL_YOUT_H) and 0x3E (ACCEL_YOUT_L)
		accelerometer_z = Wire.read() << 8 | Wire.read(); // reading registers: 0x3F (ACCEL_ZOUT_H) and 0x40 (ACCEL_ZOUT_L)
		temperature = Wire.read() << 8 | Wire.read();	  // reading registers: 0x41 (TEMP_OUT_H) and 0x42 (TEMP_OUT_L)
		gyro_x = Wire.read() << 8 | Wire.read();		  // reading registers: 0x43 (GYRO_XOUT_H) and 0x44 (GYRO_XOUT_L)
		gyro_y = Wire.read() << 8 | Wire.read();		  // reading registers: 0x45 (GYRO_YOUT_H) and 0x46 (GYRO_YOUT_L)
		gyro_z = Wire.read() << 8 | Wire.read();		  // reading registers: 0x47 (GYRO_ZOUT_H) and 0x48 (GYRO_ZOUT_L)

		totalX += accelerometer_x;
		totalY += accelerometer_y;
		totalZ += accelerometer_z;
	}

	// print out data
	Serial.printf("aX = %8.0lf | aY = %8.0lf |aZ = %8.0lf", totalX / count, totalZ / count, totalY / count); // Serial.print(convert_int16_to_str(accelerometer_x));
	// the following equation was taken from the documentation [MPU-6000/MPU-6050 Register Map and Description, p.30]
	Serial.printf(" | tmp = %.1lf", temperature / 340.00 + 36.53);
	Serial.printf(" | gX = %4d  | gY = %4d  | gZ = %4d ", gyro_x, gyro_y, gyro_z);
	Serial.println();
	// delay
	//delay(100);
}