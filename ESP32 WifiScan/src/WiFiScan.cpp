///////////////////////////////////////////////////////////////////////////////
//  This sketch demonstrates how to scan WiFi networks.
//  The API is almost the same as with the WiFi Shield library,
//  the most obvious difference being the different file you need to include:
 
#include <Arduino.h>
#include "WiFi.h"
#include <TFT_eSPI.h>

///////////////////////////////////////////////////////////////////////////////
// Result of call
class ScanResult
{
public:
	String SSID;
	int RSSI;
	wifi_auth_mode_t EncryptionType;
};

// Buttons
#define BUTTON1PIN 35
#define BUTTON2PIN 0
//#define LED (12)

// Interupt buttons
void IRAM_ATTR onButton1();
void IRAM_ATTR onButton2();
void scan();
void showNetwork(int i);
bool debounce();

// Perminent data between sleeps
RTC_DATA_ATTR int _bootCount = 0;

// deep sleep
//#define uS_TO_S_FACTOR 1000000 // conversion factor for micro seconds to seconds

TFT_eSPI tft = TFT_eSPI(); // TFT object

int _h, _w, _networkCount;
int _displayedNetwork = -1;
int _scanCount = 0;

// Array of scan results
ScanResult *_scanResults = NULL;

// Offset at bottom of screen
#define BAR (25)

// Animation values
int _cx, _cy;
int _color = 0;
#define RAINBOW_H 20.0

///////////////////////////////////////////////////////////////////////////////
// Load the inital stuff
void setup()
{
	_bootCount++;
	Serial.begin(115200);
	Serial.println("Begin setup");

	// Setup button presses
	pinMode(BUTTON1PIN, INPUT);
	pinMode(BUTTON2PIN, INPUT);
	attachInterrupt(BUTTON1PIN, onButton1, FALLING);
	attachInterrupt(BUTTON2PIN, onButton2, FALLING);

	// Allow watch dog timer to stop if we are pressing too many buttons
	//CONFIG_ESP_INT_WDT_TIMEOUT_MS

	// Initialise display
	tft.begin();
	tft.setTextDatum(TL_DATUM); // Middle centre datum
	tft.setRotation(3);
	tft.println(_bootCount);

	_w = tft.width();
	_h = tft.height();
	tft.fillRect(0, 0, _w / 2, tft.height(), TFT_BLUE);
	tft.fillRect(_w / 2, 0, _w / 2, tft.height(), TFT_RED);
	_cx = _w / 2;
	_cy = _h - RAINBOW_H - BAR - 2;

	// Set WiFi to station mode and disconnect from an AP if it was previously connected
	WiFi.mode(WIFI_STA);
	WiFi.disconnect();
	delay(100);

	//pinMode(LED, OUTPUT);

	Serial.println("Setup done");
	tft.setFreeFont(&Orbitron_Light_24);
	tft.setCursor(0, 30);
	tft.print("Setup complete");
}

// INTRPT Function to execute when Button 1 is Pushed
void IRAM_ATTR onButton1()
{
	Serial.print("onButton1:");
	Serial.println(_displayedNetwork);

	if (debounce())
		return;
	showNetwork(_displayedNetwork-1);
}
void IRAM_ATTR onButton2()
{
	Serial.print("onButton2:");
	Serial.println(_displayedNetwork);

	if (debounce())
		return;
	showNetwork(_displayedNetwork+1);
}

int _lastButtonPress;
///////////////////////////////////////////////////////////////////////////////
// Prevent buttons being pressed more often than expected
bool debounce()
{
	if (millis() - _lastButtonPress < 200)
		return true;
	_lastButtonPress = millis();
	return false;
}

String padRight(String val, int len, char padChar = ' ')
{
	String result = val;
	while (result.length() < len)
		result += padChar;
	return result;
}
String padRight(int val, int len, char padChar = ' ')
{
	String result;
	result += val;
	while (result.length() < len)
		result += padChar;
	return result;
}
String padLeft(int val, int len, char padChar = ' ')
{
	String result;
	result += val;
	String pad = padRight("", len - result.length());
	return pad + result;
}

///////////////////////////////////////////////////////////////////////////////
// Scan for new data
void scan()
{
	tft.setFreeFont(&Orbitron_Light_32);
	tft.setCursor(0, 34);

	_networkCount = 0;
	if (_scanResults)
		delete[] _scanResults;
	_scanResults = NULL;

	_scanCount++;
	// Draw in the OLED
	tft.fillScreen(TFT_BLACK);
	tft.setTextColor(TFT_WHITE, TFT_BLACK);
	tft.setCursor(0, 30);
	tft.println("Start scan");

	Serial.println("====================================================");
	Serial.print("Scan start...");
	Serial.println(millis());

	// WiFi.scanNetworks will return the number of networks found
	int networkCount = WiFi.scanNetworks();
	Serial.printf("Scan done. %d networks\r\n", networkCount);

	switch (_scanCount % 3)
	{
	case 0:
		tft.fillScreen(TFT_BLUE);
		tft.setTextColor(TFT_WHITE, TFT_BLACK);
		break;
	case 1:
		tft.fillScreen(TFT_GREEN);
		tft.setTextColor(TFT_BLACK, TFT_BLACK);
		break;

	default:
		tft.fillScreen(TFT_BLACK);
		tft.setTextColor(TFT_BLUE, TFT_BLACK);
		break;
	}
	tft.setCursor(0, 34);
	tft.printf("%d wifi\r\n", networkCount);

	// Save the data for later
	if (networkCount > 0)
	{
		int RAD = 12;
		int GAP = 2 * RAD + 3;
		int maxColumns = ( _w - RAD ) / GAP;
		_scanResults = new ScanResult[networkCount];
		for( int n = 0; n < networkCount; n++ )
		{
			// Save data
			_scanResults[n].SSID = WiFi.SSID(n);
			_scanResults[n].RSSI = WiFi.RSSI(n);
			_scanResults[n].EncryptionType = WiFi.encryptionType(n);

			// Draw a circle for each network found
			int x = GAP * ( 1 + ( n % maxColumns ) ) - RAD;
			int y = 50 + ( GAP * (int)( n / maxColumns ) );

			tft.fillCircle( x, y, RAD, TFT_RED );
			tft.drawCircle( x, y, RAD, TFT_PINK );
		}
		_networkCount = networkCount;
	}

	// Display we are thinking 
	for( int n = 0; ( n < 15 && _displayedNetwork < 0 ); n++)
	{
		tft.print(".");
		Serial.print(".");
		delay(500);
	}
}

///////////////////////////////////////////////////////////////////////////////
// Show the selected network
void showNetwork(int i)
{
	// Check for overflow
	if (_networkCount <= 0)
	{
		_displayedNetwork = -1;
		return;
	}
	i = max(0, min(i, _networkCount-1));
	_displayedNetwork = i;

	// Draw in the OLED
	tft.setFreeFont(&Orbitron_Light_24);
	tft.setCursor(0, 30);
	//tft.fillScreen(TFT_BLACK);
	tft.fillRect(0, 0, _w, _h - BAR, TFT_BLACK);
	tft.setTextColor(TFT_GREEN, TFT_BLACK);
	tft.setCursor(0, 30);

	// No networks
	if (_networkCount < 0)
	{
		tft.println("No networks found");
		tft.println("Press down to rescan");
		return;
	}
	//digitalWrite(LED, i % 2 ? HIGH : LOW);

	// Make signal strength
	int rssi = _scanResults[i].RSSI;
	String rssiText = "Unusable";
	if (rssi > -30)
		rssiText = "Amazing";
	else if (rssi > -67)
		rssiText = "Very good";
	else if (rssi > -70)
		rssiText = "OK";
	else if (rssi > -80)
		rssiText = "Not good";

	// Print SSID and RSSI for each network found
	Serial.printf("%d: %s", i + 1, padRight(_scanResults[i].SSID, 20).c_str());
	Serial.printf("(%s) %s", padLeft(rssi, 4).c_str(), rssiText.c_str());

	// Encryption type
	String encry = "Cypt:";
	encry += _scanResults[i].EncryptionType;
	switch (_scanResults[i].EncryptionType)
	{
	case WIFI_AUTH_OPEN:
		encry = ("Open");
		break;
	case WIFI_AUTH_WEP:
		encry = ("WEP");
		break;
	case WIFI_AUTH_WPA2_ENTERPRISE:
		encry = ("WPA2 Enterprise");
		break;
	case WIFI_AUTH_WPA2_PSK:
		encry = ("WPA2 PSK");
		break;
	case WIFI_AUTH_WPA_PSK:
		encry = ("WPA PSK");
		break;
	case WIFI_AUTH_WPA_WPA2_PSK:
		encry = ("WPA WPA2 PSK");
		break;
	case WIFI_AUTH_MAX:
		encry = ("MAX");
		break;
	}
	Serial.println(encry);

	// OLED display
	tft.println(_scanResults[i].SSID);

	tft.setTextColor(TFT_WHITE, TFT_BLACK);
	tft.printf("%s %d\r\n", rssiText.c_str(), _scanResults[i].RSSI);

	tft.setTextColor(TFT_GREENYELLOW, TFT_BLACK);
	tft.println(encry);

	// Progress bar
	//tft.setTextColor(TFT_GREEN, TFT_BLACK);
	//String count = "No ";
	//count += (i+1);
	//count += " of ";
	//count += n;
	//tft.println( count );

	tft.setTextColor(TFT_WHITE, TFT_BLACK);
	tft.printf("%d of %d", (i + 1), _networkCount);

	int block = (_w - 3) * 1.0 / _networkCount;
	tft.fillRect(1, _h - BAR + 1, _w - 2, BAR - 2, TFT_DARKGREY);
	tft.fillRect(i * block + 1, _h - BAR + 2, block, BAR - 3, TFT_RED);
}

int getColor(int r, int g, int b)
{
	return (((31 * (r + 4)) / 255) << 11) | (((63 * (g + 2)) / 255) << 5) | ((31 * (b + 4)) / 255);
}

//input: ratio is between 0 to 1
//output: rgb color
unsigned int rgb(double ratio)
{
	if (ratio > 1.0)
		ratio -= 1.0;

	//we want to normalize ratio so that it fits in to 6 regions
	//where each region is 256 units long
	int normalized = int(ratio * 256 * 6);

	//find the distance to the start of the closest region
	int x = normalized % 256;

	int red = 0, grn = 0, blu = 0;
	switch (normalized / 256)
	{
	case 0:
		red = 255;
		grn = x;
		blu = 0;
		break; //red
	case 1:
		red = 255 - x;
		grn = 255;
		blu = 0;
		break; //yellow
	case 2:
		red = 0;
		grn = 255;
		blu = x;
		break; //green
	case 3:
		red = 0;
		grn = 255 - x;
		blu = 255;
		break; //cyan
	case 4:
		red = x;
		grn = 0;
		blu = 255;
		break; //blue
	case 5:
		red = 255;
		grn = 0;
		blu = 255 - x;
		break; //magenta
	}

	return getColor(red, grn, blu);
}

///////////////////////////////////////////////////////////////////////////////
// Main loop
#define COLOUR_STEP 500
int _loopCount = 0;
int _direction = COLOUR_STEP;
void loop()
{
	if (_displayedNetwork < 0)
	{
		scan();
		delay(2000);
	}

	// Skip to slow things down a bit
	if (_loopCount + 10 > millis())
		return;
	_loopCount = millis();

	// Draw the spinner
	_color += _direction;
	if (_color > 0xFFFF)
		_direction = -COLOUR_STEP;
	if (_color < 1)
		_direction = COLOUR_STEP;
	for (double x = _cx; x < _w; x++)
		tft.drawLine(x, _cy, x, _cy + RAINBOW_H, rgb(((double)_color + 200 * (x - _cx)) / (double)0xFFFF));
}

// Sleep for a bit (Timer)
//esp_sleep_enable_timer_wakeup(10 * uS_TO_S_FACTOR);
//esp_sleep_enable_touchpad_wakeup();
//esp_deep_sleep_start();

// Sleep till GPIO pin trigger
//esp_sleep_enable_ext0_wakeup(GPIO_NUM_35,0);
//esp_deep_sleep_start();

// Wait a bit before scanning again
//for (int i = 0; i < 50; i++)
//{
//	Serial.print('.');
//	delay(500);
//}
//Serial.println("\r                                                ");
